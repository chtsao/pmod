+++
title = 'ykt.077.乙巳金蛇, 退妖散魔'
date = 2025-01-31T22:14:09+08:00
draft = false
categories = ["YesKno"]
tags = ["ykt",  "NewYear", "Newton's Method"]
+++

![](https://chtsao.gitlab.io/pmod/infinity.png)

🗣️  [YesKno時間 歡迎指教](https://forms.gle/4Qvjv5Ui63Nveark7)

## 乙巳金蛇, 退妖散魔

* YesKno 時間@[Youtube](https://www.youtube.com/playlist?list=PLCMDemtx7aFLQI6cxzSdodF-O6AnhViXE) 與 [短影音-極短剪](https://youtube.com/playlist?list=PLCMDemtx7aFI2HsX1pFUnwBQN790-zu0P&si=Uck1RPQtQrLR0K2k)
* 蛇祥如意
* 土地公的女兒--拜土地公、地基主
* 機率遊戲
* AI 與牛頓法
* IdeaCast@HBR: [To Fix Broken Work Systems, You Need to Reset](https://hbr.org/podcast/2025/01/to-fix-broken-work-systems-you-need-to-reset)
* 花絮：咚咚龍鏹

### Copyright Disclaimer
Cover visual  linked from [https://pixabay.com/illustrations/infinity-symbol-mathematics-math-7237232/](https://pixabay.com/illustrations/infinity-symbol-mathematics-math-7237232/)