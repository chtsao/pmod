+++
title = 'ykt.061.奧運金牌後之決策分析'
date = 2024-10-11T20:53:43+08:00
draft = false
tags =["ykt", "一次領", "按月分領","兩千萬"]
categories = ["ykt"]
+++
![](https://chtsao.gitlab.io/pmod/olympicgold.jpg)

:🎧 [先聽為快](https://open.spotify.com/show/1KlWNRx9QIu7m7JpqlIBhL?si=a1bd701af6a54627) : [Youtube](https://www.youtube.com/playlist?list=PLCMDemtx7aFLQI6cxzSdodF-O6AnhViXE) 與 [短影音-極短剪](https://youtube.com/playlist?list=PLCMDemtx7aFI2HsX1pFUnwBQN790-zu0P&si=Uck1RPQtQrLR0K2k)
🗣️  [YesKno時間 歡迎指教](https://forms.gle/4Qvjv5Ui63Nveark7)

## 奧運金牌後之決策分析

* YesKno 時間上 [Youtube](https://www.youtube.com/playlist?list=PLCMDemtx7aFLQI6cxzSdodF-O6AnhViXE) 與 [短影音-極短剪](https://youtube.com/playlist?list=PLCMDemtx7aFI2HsX1pFUnwBQN790-zu0P&si=Uck1RPQtQrLR0K2k)
* 兩千萬一次領 vs. 每月12萬五
* [ykt 13. 財務自由不是夢--不只是理財](https://chtsao.gitlab.io/pmod/posts/ep13.finindep/)
* Kno: 全領難守，自己的投資知識與素養可能不足
* Yes: 桶金難得，一次領穩健投資，可以更掌握善用這桶金
* 富不過三代的啟示，生命表的整體與個別
* Loss function, Utility 與 [Pascal wager](https://en.wikipedia.org/wiki/Pascal%27s_wager)
* 警語：Yes 與 Kno 均非財務理財背景，聽眾敬請依個人偏好與背景評估相關風險。

Cover art linked from [https://www.vogue.com.tw/entertainment/article/tokyo-olympics-mens-doubles-we-win-gold-medal](https://www.vogue.com.tw/entertainment/article/tokyo-olympics-mens-doubles-we-win-gold-medal)
