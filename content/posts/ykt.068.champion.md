+++
title = 'ykt.068.冠軍。12強。我們'
date = 2024-11-29T13:16:03+08:00
draft = false
categories = ["YesKno"]
tags = ["ykt", "棒球","冠軍","運動"]
+++
![](https://chtsao.gitlab.io/pmod/champion.jpg)

:🎧 [先聽為快](https://open.spotify.com/show/1KlWNRx9QIu7m7JpqlIBhL?si=a1bd701af6a54627) : [Youtube](https://www.youtube.com/playlist?list=PLCMDemtx7aFLQI6cxzSdodF-O6AnhViXE) 與 [短影音-極短剪](https://youtube.com/playlist?list=PLCMDemtx7aFI2HsX1pFUnwBQN790-zu0P&si=Uck1RPQtQrLR0K2k)
🗣️  [YesKno時間 歡迎指教](https://forms.gle/4Qvjv5Ui63Nveark7)

## 冠軍。12強。我們

* YesKno 時間@[Youtube](https://www.youtube.com/playlist?list=PLCMDemtx7aFLQI6cxzSdodF-O6AnhViXE) 與 [短影音-極短剪](https://youtube.com/playlist?list=PLCMDemtx7aFI2HsX1pFUnwBQN790-zu0P&si=Uck1RPQtQrLR0K2k)
* [WBSC 2024 12強冠軍!](https://www.wbsc.org/en/news/chinese-taipei-beat-japan-premier12-championship-game-reaction)
* Yes, Kno 對Team Taiwan 的默默貢獻
* 運動精神、態度與素質
* 王建民 跨世代的任務與傳承
* 我們的運動日常
* 期待東華應數壘球隊再現
* 小心路上的動物--孔雀？
* 雄女百米紀錄 與 多類型球靶
* 花絮：[花心](https://www.kkbox.com/tw/en/song/8t2kiVfjsnPsHGS6pv) by [周華健](https://www.kkbox.com/tw/en/artist/Gt1DjE-Fw6HSBCykb9) 

### Copyright Disclaimer
Cover art linked from [https://imgcdn.cna.com.tw/www/WebPhotos/1024/20241125/1200x800_wmky_830812850378_202411240240000000.jpg](https://imgcdn.cna.com.tw/www/WebPhotos/1024/20241125/1200x800_wmky_830812850378_202411240240000000.jpg)