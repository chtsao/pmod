+++
title = 'ykt.066.你問問題嗎？'
date = 2024-11-16T07:34:30+08:00
draft = false
categories = ["ykt"]
tags = ["question","問題"]
+++
![](https://chtsao.gitlab.io/pmod/question.jpg)

:🎧 [先聽為快](https://open.spotify.com/show/1KlWNRx9QIu7m7JpqlIBhL?si=a1bd701af6a54627) : [Youtube](https://www.youtube.com/playlist?list=PLCMDemtx7aFLQI6cxzSdodF-O6AnhViXE) 與 [短影音-極短剪](https://youtube.com/playlist?list=PLCMDemtx7aFI2HsX1pFUnwBQN790-zu0P&si=Uck1RPQtQrLR0K2k)
🗣️  [YesKno時間 歡迎指教](https://forms.gle/4Qvjv5Ui63Nveark7)

## 你問問題嗎？

* YesKno 時間@[Youtube](https://www.youtube.com/playlist?list=PLCMDemtx7aFLQI6cxzSdodF-O6AnhViXE) 與 [短影音-極短剪](https://youtube.com/playlist?list=PLCMDemtx7aFI2HsX1pFUnwBQN790-zu0P&si=Uck1RPQtQrLR0K2k)
* 某人最近教室比較。。安靜。。？
* 問問題：一問三不知
* 千金難買早知道
* 輸出練習
* 主動與被動的學習 [ykt. 26./kbc. 06. 檢視你的學習方式--更有效率而聰明的學習](https://chtsao.gitlab.io/pmod/posts/ep026.studysmart/)
* 彩蛋 [如鹿覓溪水] 高山阿嬤

### Copyright Disclaimer
Cover art linked from [https://wallpapers.com/images/hd/question-mark-background-6lh3o1mwmmso80dx.jpg](https://wallpapers.com/images/hd/question-mark-background-6lh3o1mwmmso80dx.jpg)