+++
title = 'ykt.059.不只是聽演講--關於研討會和...'
date = 2024-09-28T06:38:02+08:00
draft = false
tags =["ykt", "演講", "研討會"]
categories = ["ykt"]
+++
![](https://chtsao.gitlab.io/pmod/conference.jpg)

:🎧 [先聽為快](https://open.spotify.com/show/1KlWNRx9QIu7m7JpqlIBhL?si=a1bd701af6a54627) :  🗣️  [YesKno時間 歡迎指教](https://forms.gle/4Qvjv5Ui63Nveark7)

## 不只是聽演講--關於研討會和...

* seminars, conferences, workshop
* 甜甜圈與國家院士
* 大大型研討會面面觀
* 把握面對面交流與溝通機會
* 走過了解幕後，幕前看得更清楚

Cover art linked from [https://www.brandknewmag.com/which-design-conferences-are-best-for-designers/](https://www.brandknewmag.com/which-design-conferences-are-best-for-designers/)
