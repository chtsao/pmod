+++
title = 'ykt.70.就學貸款--你為什麼應該貸'
date = 2024-12-13T20:25:51+08:00
draft = false
categories = ["YesKno"]
tags = ["ykt", "就學貸款","理財","機會成本","當令"]
+++
![](https://chtsao.gitlab.io/pmod/sloan.jpg)

:🎧 [先聽為快](https://open.spotify.com/show/1KlWNRx9QIu7m7JpqlIBhL?si=a1bd701af6a54627) : [Youtube](https://www.youtube.com/playlist?list=PLCMDemtx7aFLQI6cxzSdodF-O6AnhViXE) 與 [短影音-極短剪](https://youtube.com/playlist?list=PLCMDemtx7aFI2HsX1pFUnwBQN790-zu0P&si=Uck1RPQtQrLR0K2k)
🗣️  [YesKno時間 歡迎指教](https://forms.gle/4Qvjv5Ui63Nveark7)

## 就學貸款--你為什麼應該貸

* YesKno 時間@[Youtube](https://www.youtube.com/playlist?list=PLCMDemtx7aFLQI6cxzSdodF-O6AnhViXE) 與 [短影音-極短剪](https://youtube.com/playlist?list=PLCMDemtx7aFI2HsX1pFUnwBQN790-zu0P&si=Uck1RPQtQrLR0K2k)
* [113學年度大專校院助學措施手冊](https://ws.moe.edu.tw/Download.ashx?u=C099358C81D4876CA918C850851551F80A3E1FD3022B47688BCFF942F003BF60BC2D635B272329F801C0659EAD2C649861CB298F2DB060FE2D43342D7F4788939B7AD713BCF335F3154FE5851F00FFCB&n=1B8E4078D106156996AC08AC7323B1AF241DA122DBC62178D4C4CDA2891002327EF1A0D78BC603B5DABF56E38E933A14&icon=..pdf)
* 善用資源，從大學第一天開始就把錢從桌上拿開
* 現在與未來的一塊錢是不同的，一個鐘頭也完全不一樣
* 機會成本：不要把你價值最高的時間 交換成可能是你一生最低的工資
* 時機  C/P 值 與 當令 當時 
* 經驗曲線：[張忠謀自傳(下)](https://www.sanmin.com.tw/product/index/013634912)
* [ykt 13. 財務自由不是夢--不只是理財 ](https://chtsao.gitlab.io/pmod/posts/ep13.finindep/)
* 花絮：[我家在那裡](https://www.kkbox.com/tw/en/song/-pfLsS6hzoZVr5UiZ9) by [鳳飛飛](https://www.kkbox.com/tw/en/artist/4nrdwg-K2KLL6xgk7-) 

### Copyright Disclaimer
Cover art linked from [https://www.vantagescore.com/vantagescore-unveils-student-loan-payment-resources/](https://www.vantagescore.com/vantagescore-unveils-student-loan-payment-resources/)