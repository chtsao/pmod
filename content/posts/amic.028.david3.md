+++
title = 'amic.028.David之給大一'
date = 2024-10-06T04:41:17+08:00
draft = false
tags =["amic", "大一", "拱豬大賽"]
categories = ["amic"]
+++
![](https://chtsao.gitlab.io/pmod/hg.jpg)

:🎧 [先聽為快](https://open.spotify.com/show/6EOiyanrZTWWxpidZlIyb1?si=bdc4c9ce66be4b59)
:🗣️  [A麥開講,歡迎指教](https://forms.gle/N29BRtvDezbKo6Vx8)

## David之給大一

* 大一生活如何過？可以充實、可以荒唐、但更可以多采多姿
* 包子與蛋糕 [演習課與導師時間](https://am.ndhu.edu.tw/p/412-1038-2933.php?Lang=zh-tw): Yes: MON & THU 12:10-13:00; Kno: TUE & THU 12:10-13:00
* 傳說中的拱豬大賽 10/22 等你來抓豬補羊
* 在魔法學院裏，只要你開口，就會得到幫助

Cover art linked from [Harry Potter Hogwarts Castle](https://xfxwallpapers.blogspot.com/2019/12/harry-potter-hogwarts-castle.html)
