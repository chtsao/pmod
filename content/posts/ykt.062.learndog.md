+++
title = 'ykt.062.好狗狗是慢慢教慢慢養出來的'
date = 2024-10-18T20:26:59+08:00
draft = false
tags =["ykt", "狗狗", "模範","pack"]
categories = ["ykt"]
+++
![](https://chtsao.gitlab.io/pmod/packof4.jpg)

:🎧 [先聽為快](https://open.spotify.com/show/1KlWNRx9QIu7m7JpqlIBhL?si=a1bd701af6a54627) : [Youtube](https://www.youtube.com/playlist?list=PLCMDemtx7aFLQI6cxzSdodF-O6AnhViXE) 與 [短影音-極短剪](https://youtube.com/playlist?list=PLCMDemtx7aFI2HsX1pFUnwBQN790-zu0P&si=Uck1RPQtQrLR0K2k)
🗣️  [YesKno時間 歡迎指教](https://forms.gle/4Qvjv5Ui63Nveark7)

## 好狗狗是慢慢教慢慢養出來的

* YesKno 時間上 [Youtube](https://www.youtube.com/playlist?list=PLCMDemtx7aFLQI6cxzSdodF-O6AnhViXE) 與 [短影音-極短剪](https://youtube.com/playlist?list=PLCMDemtx7aFI2HsX1pFUnwBQN790-zu0P&si=Uck1RPQtQrLR0K2k)
* 我們家的模範小狗：不過於敏感，作息同步，不亂闖，與人適當互動
* 如何養成好小狗：從認知開始-- 一輩子的相處與責任
* 狗狗的一輩子：可愛期，尷尬期，成熟期，資深期
* 自然非暴力地讓狗狗認知牠在 pack 中的位置 
* 基本指令：坐下，等一等，Down, Give me five; 微分，積分
* 中大型犬及早教會 不要撲人 與 口齒慎用 [阿嬤手勢](https://blog.homesalive.ca/dog-blog/hand-signals-for-dogs)
* 其他：狗與狗第一次見面，及早讓狗狗與其他狗以及人有互動經驗，給狗與狗有自行解決問題的空間
