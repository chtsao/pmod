+++
title = 'ykt 11. (kbc 01) 大數法則與中央極限定理- Kno外掛之ㄧ'
date = 2024-01-10T08:58:31+08:00
draft = false
tags = ["prob", "stat","CLT", "LLN", "kbc"]
categories =["kbc"]
author ="Kno"

+++
[先聽為快](https://player.soundon.fm/p/43e61ad9-d1bf-43f1-aebe-fb5f502bad61/episodes/0cc59e56-b78b-4a68-a39a-dc8f70adb7f6)

趁 Yes 不在家，Kno 偷開外掛首集--kbc (K書中心) 01. 

* 挑戰! 30 分鐘內兩大定理：Law of Large Numbers 與 Central Limit Theorem
* 挑戰! 你在 30分鐘 內的忍睡度
* 難以入眠朋友們睡前最佳BGM
* 你可以挑出 AI 的錯誤嗎？[Asymptotics](https://chtsao.gitlab.io/i2p2023/posts/xbar/#asymptotics)

