+++
title = 'ykt.052.yessta04.關於推甄--Yes視角'
date = 2024-08-10T01:45:45+08:00
draft =false
categories=["yesta", "ykt"]
tags= ["聰明工作", "努力", "績效","project"]
author=["Kno"]

+++
![](https://chtsao.gitlab.io/pmod/stairs.jpg)


:🎧 [先聽為快](https://open.spotify.com/show/1KlWNRx9QIu7m7JpqlIBhL?si=a1bd701af6a54627) :  🗣️  [YesKno時間 歡迎指教](https://forms.gle/4Qvjv5Ui63Nveark7)

# 關於推甄--Yes視角 

* 推甄的本質–成績好壞影響
* 各研究所從推薦信中想知道什麼？
* 自傳/讀書計畫 其實是 你為自己寫的推薦信
* GPA 不是重點，相關重點修課表現才是
* 如何使推薦函更有力? 
* 面試提醒 更多在 [ykt 27./yessta 01. 研究所甄試口試準備重點](https://chtsao.gitlab.io/pmod/posts/ep027.yessta.01.oralexam/)
* 片尾彩蛋：夜空下最亮的星 by 逃跑計畫 covered by 張張、江江

#### Copyright disclaimer

Visual borrowed from [https://wallhere.com/en/wallpaper/1534161](https://c.wallhere.com/photos/b2/22/artwork_optical_illusion_M_C_Escher_drawing_monochrome_stairs_building_indoors-1534161.jpg!d)