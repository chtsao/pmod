+++
title = 'ykt.071.歲末趕場：趕課、相聲、崑曲、爵士、桌游'
date = 2024-12-21T05:51:11+08:00
draft = false
categories = ["YesKno"]
tags = ["ykt", "相聲","崑曲","爵士","桌游"]
+++
![](https://chtsao.gitlab.io/pmod/kuan.jpg)
:🎧 [先聽為快](https://open.spotify.com/show/1KlWNRx9QIu7m7JpqlIBhL?si=a1bd701af6a54627) : [Youtube](https://www.youtube.com/playlist?list=PLCMDemtx7aFLQI6cxzSdodF-O6AnhViXE) 與 [短影音-極短剪](https://youtube.com/playlist?list=PLCMDemtx7aFI2HsX1pFUnwBQN790-zu0P&si=Uck1RPQtQrLR0K2k)
🗣️  [YesKno時間 歡迎指教](https://forms.gle/4Qvjv5Ui63Nveark7)
## 歲末趕場：趕課、相聲、崑曲、爵士、桌游

* YesKno 時間@[Youtube](https://www.youtube.com/playlist?list=PLCMDemtx7aFLQI6cxzSdodF-O6AnhViXE) 與 [短影音-極短剪](https://youtube.com/playlist?list=PLCMDemtx7aFI2HsX1pFUnwBQN790-zu0P&si=Uck1RPQtQrLR0K2k)
* 趕課、趕進度
* 相聲：[朱德剛相聲社](https://www.facebook.com/CTK.crosstalk.teahouse/)
* 崑曲：[台灣崑曲團](https://taiwankunju.com/)：[牡丹亭](https://zh.wikipedia.org/wiki/%E7%89%A1%E4%B8%B9%E4%BA%AD)：<驚夢>，<幽媾>；水滸記：<借茶> ， <活捉>  之 [崑曲地下情]( https://artscenter.ndhu.edu.tw/p/406-1123-234012,r5125.php?Lang=zh-tw) @[東華藝文中心](https://artscenter.ndhu.edu.tw/index.php)
* Jazz Big Band：[Stacey Wei](https://www.facebook.com/staceyweijazz/), [Count Bassie](https://www.kkbox.com/tw/en/artist/4sYpPFdLYqFkCQZePs)
* 桌游社歲末大會
* Kno 魔術社魔術驗收表演之最佳舞台獎
* 花絮：[蘇三起解](https://www.kkbox.com/tw/en/song/0l170H4ns1XUe5LbbZ) by [李翊君](https://www.kkbox.com/tw/en/artist/5ZchIWda-SSccF3bBn) 

### Copyright Disclaimer
Cover art linked from [https://read01.com/d0KQjNa.html](https://read01.com/d0KQjNa.html)