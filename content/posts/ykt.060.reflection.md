+++
title = 'ykt.060.六十集回顧上Youtube'
date = 2024-10-04T21:05:56+08:00
draft = false
tags =["ykt", "youtube", "短影音"]
categories = ["ykt"]
+++
![](https://chtsao.gitlab.io/pmod/podcast.jpg)

:🎧 [先聽為快](https://open.spotify.com/show/1KlWNRx9QIu7m7JpqlIBhL?si=a1bd701af6a54627) :  🗣️  [YesKno時間 歡迎指教](https://forms.gle/4Qvjv5Ui63Nveark7)

## 六十集回顧上Youtube

* YesKno 時間上 [Youtube](https://www.youtube.com/playlist?list=PLCMDemtx7aFLQI6cxzSdodF-O6AnhViXE) 與 [短影音-極短剪](https://youtube.com/playlist?list=PLCMDemtx7aFI2HsX1pFUnwBQN790-zu0P&si=Uck1RPQtQrLR0K2k)
* Kno 之外掛
* 器材磨合 與 開嗆 台積電 ;)
* 太正式也許反而不容易走好
* 唱歌彩蛋--唱 彈 溝通與默契
* YesKnotes Shownotes 準備 tags 整理與處理
* 六十集快轉

Cover art "Transforming coffee to podcasts" taken by Kno.
