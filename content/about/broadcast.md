+++
title = 'YesKno時間 播放表'
date = 2024-01-03T07:20:54+08:00
draft = false
tags =["broadcast", "Podcast"]

+++
![](https://chtsao.gitlab.io/pmod/yesknotime.fb.png)

* [SoundOn](https://player.soundon.fm/p/43e61ad9-d1bf-43f1-aebe-fb5f502bad61), [Spotify](https://open.spotify.com/show/3Htz83kbcRkIqhusWR6VcP?si=bc0e7212a6b6411b),  [KKBox](https://podcast.kkbox.com/tw/channel/CkW1g6ZHqduSUpTda3), [Apple Podcast](https://podcasts.apple.com/tw/podcast/yeskno-%E6%99%82%E9%96%93/id1723126723)
* [Youtube](https://www.youtube.com/playlist?list=PLCMDemtx7aFLQI6cxzSdodF-O6AnhViXE) 與 [短影音-極短剪](https://youtube.com/playlist?list=PLCMDemtx7aFI2HsX1pFUnwBQN790-zu0P&si=Uck1RPQtQrLR0K2k)
* [Listen Notes](https://lnns.co/dNu0Yi_BTcq)

### Shownotes
* [YesKnotes](https://chtsao.gitlab.io/pmod/)

### 友台

[<img src="https://images.weserv.nl/?il&fit=contain&w=200&h=200&dpr=1&url=https://files.soundon.fm/1703605375535-39dc489c-c98a-4b96-a549-9bf63e567878.jpeg"  />](https://player.soundon.fm/p/b2faa4e2-1f4e-41b0-aff3-72e7fcc04263)[<img src="https://images.weserv.nl/?il&fit=contain&w=300&h=300&dpr=1&url=https://files.soundon.fm/1679162548378-6ebbc0c4-179e-45da-a26b-6562aee15d5e.jpeg" style="zoom: 40%;" />](https://player.soundon.fm/p/e4c62dc9-54b1-4d2b-8f0e-71ca4363eaf4)
