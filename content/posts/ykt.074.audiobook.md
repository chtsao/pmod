+++
title = 'ykt.074.有聲書試讀'
date = 2025-01-10T19:50:43+08:00
draft  = false
categories = ["YesKno"]
tags = ["ykt",  "有聲書", "好讀"]
+++

![](https://chtsao.gitlab.io/pmod/audiobook.jpg)

🗣️  [YesKno時間 歡迎指教](https://forms.gle/4Qvjv5Ui63Nveark7)

## 有聲書試讀

* YesKno 時間@[Youtube](https://www.youtube.com/playlist?list=PLCMDemtx7aFLQI6cxzSdodF-O6AnhViXE) 與 [短影音-極短剪](https://youtube.com/playlist?list=PLCMDemtx7aFI2HsX1pFUnwBQN790-zu0P&si=Uck1RPQtQrLR0K2k)
* 有聲書 試讀
* 阿城：棋王 @ 棋王、樹王、孩子王. 新地 當代中國大陸文學叢刊 1。
* 芥川龍之介：橘子  @ 海市蜃樓、橘子：芥川龍之介十四個短篇傑作選。譯者: 文潔若
* [好讀網站](https://www.haodoo.net/) 
* 花絮：吉他演奏：南方,寂寞火車 陳明章作曲

### Copyright Disclaimer
Cover visual  linked from [https://icon-library.com/images/audio-book-icon/audio-book-icon-23.jpg](https://icon-library.com/images/audio-book-icon/audio-book-icon-23.jpg)