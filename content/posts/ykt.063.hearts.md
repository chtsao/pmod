+++
title = 'ykt.063.拱豬、耍心機與遊戲'
date = 2024-10-26T05:30:58+08:00
draft = false
tags =["ykt", "拱豬", "心機","算計"]
categories = ["ykt"]
+++
![](https://chtsao.gitlab.io/pmod/hearts.jpg)

:🎧 [先聽為快](https://open.spotify.com/show/1KlWNRx9QIu7m7JpqlIBhL?si=a1bd701af6a54627) : [Youtube](https://www.youtube.com/playlist?list=PLCMDemtx7aFLQI6cxzSdodF-O6AnhViXE) 與 [短影音-極短剪](https://youtube.com/playlist?list=PLCMDemtx7aFI2HsX1pFUnwBQN790-zu0P&si=Uck1RPQtQrLR0K2k)
🗣️  [YesKno時間 歡迎指教](https://forms.gle/4Qvjv5Ui63Nveark7)

## 拱豬、耍心機與遊戲

* YesKno 時間上 [Youtube](https://www.youtube.com/playlist?list=PLCMDemtx7aFLQI6cxzSdodF-O6AnhViXE) 與 [短影音-極短剪](https://youtube.com/playlist?list=PLCMDemtx7aFI2HsX1pFUnwBQN790-zu0P&si=Uck1RPQtQrLR0K2k)
* 耍心機與算計
* 拱豬規則之東華應數版
* 贏/不敗的策略
* 想當豬王？沒那麼簡單
* 算計與人生
### Read and Links
* [拱豬/ Hearts](https://en.wikipedia.org/wiki/Hearts_(card_game))
