+++
title = 'ykt.056.整理就緒起跑'
date = 2024-09-06T10:03:18+08:00
draft = false
tags =["ykt", "整理", "故宮文物", "花蓮"]
categories = ["ykt"]
+++
![](https://chtsao.gitlab.io/pmod/mise-en-place.jpg)

:🎧 [先聽為快](https://open.spotify.com/show/1KlWNRx9QIu7m7JpqlIBhL?si=a1bd701af6a54627) :  🗣️  [YesKno時間 歡迎指教](https://forms.gle/4Qvjv5Ui63Nveark7)

## 整理就緒起跑

* 開學前整理辦公室
* 教授辦公室裡的陳年資料與故宮文物
* 應數系重回理工一館實現！
* 東華應數 30 還是 30＋1？
* 真樸、堅韌而美麗的花蓮--我們一起加油！

Cover art linked from [https://delishably.com/food-industry/What-Is-Mise-en-Place-and-Why-Is-It-a-Must-In-Your-Kitchen](https://delishably.com/food-industry/What-Is-Mise-en-Place-and-Why-Is-It-a-Must-In-Your-Kitchen)
