+++
title = 'ykt 9. New Year 10^3+ 2^10! 研究所學什麼？'
date = 2024-01-03T06:08:14+08:00
draft = false
tags = ["研究所"]

+++
<img src="https://i.pinimg.com/originals/64/c4/4b/64c44bba513bbd1687c4a4149626fa09.jpg" style="zoom:150%;" />

[先聽為快](https://player.soundon.fm/p/43e61ad9-d1bf-43f1-aebe-fb5f502bad61/episodes/db809a9e-b5b5-4b6e-abef-b1055a98fe4f)

* 新年新思維：過去 與 未來 $10^3 + 2^{10}$
* 研究所在學什麼？
* 手中有屠龍刀，卻連隻雞都抓不著？
* 資料世代 資料分析 時代來臨 
* 你已經贏在起跑點--數理/統計/程式 背景/訓練

#### 參考閱讀

* [研究生完全求生手冊：方法、秘訣、潛規則](https://readmoo.com/book/210074238000101) by [彭明輝](https://mhperng.blogspot.com/2017/08/blog-post_15.html)



