+++
title = 'ykt.076.kbc22.過年前最好先知道的機率'
date = 2025-01-24T15:23:15+08:00
draft = false
tags = ["極限", "大數法則", "賭徒末日定理"]
categories=["kbc", "ykt"]
author = "Kno"
+++
![](https://chtsao.gitlab.io/pmod/sting.jpg)

:🎧 [先聽為快](https://open.spotify.com/show/1KlWNRx9QIu7m7JpqlIBhL?si=a1bd701af6a54627) : [Youtube](https://www.youtube.com/playlist?list=PLCMDemtx7aFLQI6cxzSdodF-O6AnhViXE) 與 [短影音-極短剪](https://youtube.com/playlist?list=PLCMDemtx7aFI2HsX1pFUnwBQN790-zu0P&si=Uck1RPQtQrLR0K2k)
🗣️  [YesKno時間 歡迎指教](https://forms.gle/4Qvjv5Ui63Nveark7)

## 過年前最好先知道的機率

* YesKno 時間@[Youtube](https://www.youtube.com/playlist?list=PLCMDemtx7aFLQI6cxzSdodF-O6AnhViXE) 與 [短影音-極短剪](https://youtube.com/playlist?list=PLCMDemtx7aFI2HsX1pFUnwBQN790-zu0P&si=Uck1RPQtQrLR0K2k)
* 絕大多數的機率都是一個極限的敘述
* 大數法則 沒有說 輸久了就會贏
* 賭徒末日定理：單看期望值，你可能當容易輸光
* [ykt 11. (kbc 01) 大數法則與中央極限定理- Kno外掛之ㄧ ](https://chtsao.gitlab.io/pmod/posts/eps11.kbc01.llnclt0/)
* [Ep45.小賭怡情？那你應該沒聽過賭徒末日定理！feat. Kno](https://podcast.kkbox.com/episode/9-SIWVrIjSq900-TCF) ＠ 黃邦亂講

### Disclaimer
Cover art linked from [https://todaytvseries.one/movies/the-sting-1973/](https://todaytvseries.one/movies/the-sting-1973/)