+++
title = 'ykt.058.看書這回事'
date = 2024-09-20T13:31:21+08:00
draft = false
tags =["ykt", "書", "麻雀"]
categories = ["ykt"]
+++
![](https://chtsao.gitlab.io/pmod/wobooks.jpg)

:🎧 [先聽為快](https://open.spotify.com/show/1KlWNRx9QIu7m7JpqlIBhL?si=a1bd701af6a54627) :  🗣️  [YesKno時間 歡迎指教](https://forms.gle/4Qvjv5Ui63Nveark7)

## 看書這回事

* 看書、找書、買書
* 讀書的斷層
* 大火猛攻、小火慢煨 余秋雨
* 一本書就是一位知者，閱讀就是與他/她的交談與討論
* 讀書 --> 有趣好玩、拓展視野，開啟另一扇窗，不同的視角與觀點
* [盧梭](https://zh.wikipedia.org/zh-tw/%E8%AE%A9-%E9%9B%85%E5%85%8B%C2%B7%E5%8D%A2%E6%A2%AD), [社會契約論](https://zh.wikipedia.org/wiki/%E7%A4%BE%E4%BC%9A%E5%A5%91%E7%BA%A6%E8%AE%BA)（也許更應該是[洛克](https://zh.wikipedia.org/wiki/%E7%BA%A6%E7%BF%B0%C2%B7%E6%B4%9B%E5%85%8B)）[天賦人權](https://www.newton.com.tw/wiki/%E5%A4%A9%E8%B3%A6%E4%BA%BA%E6%AC%8A)與麻雀之大亂鬥
* 獨立向量 空間拓展
* Yes 推書

Cover art linked from [https://crimereads.com/five-speculative-novels-set-in-worlds-full-of-books/](https://crimereads.com/five-speculative-novels-set-in-worlds-full-of-books/)
