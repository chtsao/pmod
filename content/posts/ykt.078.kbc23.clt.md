+++
title = 'ykt.078.kbc23.數大就是美？中央極限定理'
date = 2025-02-08T05:07:31+08:00
draft = false
tags = ["CLT", "central limit theorem"]
categories=["kbc", "ykt"]
author = "Kno"
+++
![](https://chtsao.gitlab.io/pmod/bw.jpg)

:🎧 [先聽為快](https://open.spotify.com/show/1KlWNRx9QIu7m7JpqlIBhL?si=a1bd701af6a54627) : [Youtube](https://www.youtube.com/playlist?list=PLCMDemtx7aFLQI6cxzSdodF-O6AnhViXE) 與 [短影音-極短剪](https://youtube.com/playlist?list=PLCMDemtx7aFI2HsX1pFUnwBQN790-zu0P&si=Uck1RPQtQrLR0K2k)
🗣️  [YesKno時間 歡迎指教](https://forms.gle/4Qvjv5Ui63Nveark7)

## 數大就是美？中央極限定理

* YesKno 時間@[Youtube](https://www.youtube.com/playlist?list=PLCMDemtx7aFLQI6cxzSdodF-O6AnhViXE) 與 [短影音-極短剪](https://youtube.com/playlist?list=PLCMDemtx7aFI2HsX1pFUnwBQN790-zu0P&si=Uck1RPQtQrLR0K2k)
* Central Limit Theorem:  Let $X_1, \cdots, X_n \sim_{iid}$ with $E(X_i)= \mu, Var(X_i) =\sigma^2$. Then $\frac{\bar{X}-\mu}{\sigma/\sqrt{n}} \rightarrow N(0,1)$ as $n$ goes to infinity where $\bar{X}=\frac{1}{n}(X_1+\cdots+X_n).$
* 在同一模式/分配下，當樣本數趨近於無限大，獨立抽樣(realized sample)的直方圖(histogram) 趨近於該分配(pmf or pdf) 也不見得是常態。這個敘述，並不是中央極限定理。而僅是說明 pmf/pdf 可以視為一個標準化後直方圖的極限；或是如何透過資料來了估計/了解他們。
* 在同一模式/分配下，當樣本數趨近於無限大，獨立抽樣平均($\bar{x}$)的直方圖趨近於常態 where $\bar{x}=\frac{1}{n}(x_1+\cdots+x_n)$, a realization of $\bar{X}.$ 注意：要讓實驗次數 $K$ 夠多，這些 $K$ 個 $\bar{x}'s$ 的直方圖，才會顯現出$\bar{X}$ 的分配。 這裡有兩個極限，這也是中央極限定理讓人困擾的一個地方。
* [ykt 11. (kbc 01) 大數法則與中央極限定理- Kno外掛之ㄧ ](https://chtsao.gitlab.io/pmod/posts/eps11.kbc01.llnclt0/)
* 沒有特別模式之下，或沒有特別規劃的抽樣下，無限多的樣本，可能只是一群不相干的數字。他們的直方圖不見得會收斂/區近於什麼特別的樣子。

### Disclaimer
Visual  linked from [https://wallhere.com/it/wallpaper/560905](https://wallhere.com/it/wallpaper/560905)







