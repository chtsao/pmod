+++
title = 'ykt.079.討厭相同比喜歡類似更重要'
date = 2025-02-28T17:59:15+08:00
draft = false
categories = ["YesKno"]
tags = ["ykt",  "討厭", "喜歡","在一起"]

+++
![](https://chtsao.gitlab.io/pmod/laser-room-2.jpg)

🗣️  [YesKno時間 歡迎指教](https://forms.gle/4Qvjv5Ui63Nveark7)

## 討厭相同比喜歡類似更重要

* YesKno 時間@[Youtube](https://www.youtube.com/playlist?list=PLCMDemtx7aFLQI6cxzSdodF-O6AnhViXE) 與 [短影音-極短剪](https://youtube.com/playlist?list=PLCMDemtx7aFI2HsX1pFUnwBQN790-zu0P&si=Uck1RPQtQrLR0K2k)
* 朋友、伴侶、夫妻間 討厭相同 比 喜歡類似 來得更重要
* 粉紅泡泡散去後---蛙化
* 對錯是非--基本價值觀
* 不要太看輕情緒 [EPS#190 - Are emotions a waste of time? - Neo-Stoicism (Martha Nussbaum)](https://www.philosophizethis.org/podcast/episode-179-consciousness-hard-problem-l8d98-td63g-47g5g-ha6yr-papmr-kaj7p-4ybpm-m83zf?rq=emotion) @ [PhilosophizeThis](https://www.philosophizethis.org/)
* 對齊紅線
* 花絮：驛動的心

### Copyright Disclaimer
Cover visual  linked from [https://www.spy-games.com/wp-content/uploads/2011/07/laser-room-2.jpg](https://www.spy-games.com/wp-content/uploads/2011/07/laser-room-2.jpg)
