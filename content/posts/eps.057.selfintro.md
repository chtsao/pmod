+++
title = 'ykt.057.自我介紹不等同於介紹自己'
date = 2024-09-13T22:19:20+08:00
draft = false
tags =["ykt", "自我介紹", "換位思考"]
categories = ["ykt"]
+++
![](https://chtsao.gitlab.io/pmod/hello.jpg)

:🎧 [先聽為快](https://open.spotify.com/show/1KlWNRx9QIu7m7JpqlIBhL?si=a1bd701af6a54627) :  🗣️  [YesKno時間 歡迎指教](https://forms.gle/4Qvjv5Ui63Nveark7)

## 自我介紹不等同於介紹自己

* 從聽眾角度思考
* 考慮說話的人地時事與主題
* 聆聽與建立個人形象/品牌
* [18秒超強自我介紹術：翻轉人生，把人脈、工作、財源通通吸過來](https://www.books.com.tw/products/E050123508)
* [自我介紹？不是要你介紹自己，而是如何留下印象](https://medium.com/y-pointer/self-introduction-2ff7fa558069)

Cover art linked from [https://webstockreview.net/explore/hello-clipart-kid-hello/](https://webstockreview.net/images/hello-clipart-kid-hello.jpg)
