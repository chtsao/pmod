+++
title = 'ykt.069.kbc.21.微積分想要做什麼？'
date = 2024-12-06T13:13:59+08:00
draft = false
tags = ["calculus", "infinity principle", "Newton's method", "art"]
categories=["kbc", "ykt"]
author = "Kno"
+++
![](https://chtsao.gitlab.io/pmod/area.circle.jpg)

:🎧 [先聽為快](https://open.spotify.com/show/1KlWNRx9QIu7m7JpqlIBhL?si=a1bd701af6a54627) : [Youtube](https://www.youtube.com/playlist?list=PLCMDemtx7aFLQI6cxzSdodF-O6AnhViXE) 與 [短影音-極短剪](https://youtube.com/playlist?list=PLCMDemtx7aFI2HsX1pFUnwBQN790-zu0P&si=Uck1RPQtQrLR0K2k)
🗣️  [YesKno時間 歡迎指教](https://forms.gle/4Qvjv5Ui63Nveark7)

## 微積分想要做什麼？

* YesKno 時間@[Youtube](https://www.youtube.com/playlist?list=PLCMDemtx7aFLQI6cxzSdodF-O6AnhViXE) 與 [短影音-極短剪](https://youtube.com/playlist?list=PLCMDemtx7aFI2HsX1pFUnwBQN790-zu0P&si=Uck1RPQtQrLR0K2k)
* [The Story of Calculus - 2022 Ulam Memorial Lecture (1/2)](https://www.youtube.com/live/S1gXA4B8DL0?feature=shared&t=490) by [Steven Strogatz](https://www.stevenstrogatz.com/)
* Calculus 想要做什麼？如何去做？
* [Divide and conquer](https://www.geeksforgeeks.org/divide-and-conquer/) to $\infty$
* 圓面積計算 與 無限原則
* [上帝所說的語言](https://galileospendulum.org/2011/01/12/the-language-god-talks/) 與 [Newton's Method](https://tutorial.math.lamar.edu/Classes/CalcI/NewtonsMethod.aspx)
* [Zeno's paradox](https://plato.stanford.edu/entries/paradox-zeno/): [Achilles and the Tortoise](https://plato.stanford.edu/entries/paradox-zeno/#AchiTort)
* [Picasso: Art is a lie that enables us to realize the truth.](https://libquotes.com/pablo-picasso/quote/lbn0k0u)

### Disclaimer
Cover art linked from [https://www.teahub.io/viewwp/ixhhhmh_warren-buffett-reading/](https://www.youtube.com/watch?v=f_Yo033w5yM/)