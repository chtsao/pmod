+++
title = 'ykt.073.129：平均。股民。獲利'
date = 2025-01-04T07:42:29+08:00
draft = false
categories = ["YesKno"]
tags = ["ykt",  "平均", "投資", "習慣"]
+++
![](https://chtsao.gitlab.io/pmod/6208.jpg)

🗣️  [YesKno時間 歡迎指教](https://forms.gle/4Qvjv5Ui63Nveark7)

## 129：平均。股民。獲利

* YesKno 時間@[Youtube](https://www.youtube.com/playlist?list=PLCMDemtx7aFLQI6cxzSdodF-O6AnhViXE) 與 [短影音-極短剪](https://youtube.com/playlist?list=PLCMDemtx7aFI2HsX1pFUnwBQN790-zu0P&si=Uck1RPQtQrLR0K2k)
* 年底盤點：[台股2024年大豐收！飆漲5104點創史高、股民平均賺逾129萬](https://ec.ltn.com.tw/article/breakingnews/4909341)
* 兩種平均：分配 (distribution) 的平均，長期期望值
* Indexed Fund 創始者：John Bogle
* 年輕者投資最重要的事---投資自己
* 慣性：好習慣的養成
* [ykt 13. 財務自由不是夢--不只是理財](https://chtsao.gitlab.io/pmod/posts/ep13.finindep/)
* 花絮：茉莉花

### Copyright Disclaimer
Cover art linked from [https://www.nskbearingcatalogue.com/wp-content/uploads/2019/01/6208-6.jpg](https://www.nskbearingcatalogue.com/product/6208-bearing/)

