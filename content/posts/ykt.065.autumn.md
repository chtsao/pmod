+++
title = 'ykt.065.秋之果、樹與紅螞蟻'
date = 2024-11-08T20:46:59+08:00
draft = false
tags =["秋","四季","樹木","過敏"]
categories = ["ykt"]
+++
![](https://chtsao.gitlab.io/pmod/4seasons.jpg)

:🎧 [先聽為快](https://open.spotify.com/show/1KlWNRx9QIu7m7JpqlIBhL?si=a1bd701af6a54627) : [Youtube](https://www.youtube.com/playlist?list=PLCMDemtx7aFLQI6cxzSdodF-O6AnhViXE) 與 [短影音-極短剪](https://youtube.com/playlist?list=PLCMDemtx7aFI2HsX1pFUnwBQN790-zu0P&si=Uck1RPQtQrLR0K2k)
🗣️  [YesKno時間 歡迎指教](https://forms.gle/4Qvjv5Ui63Nveark7)

## 秋之果、樹與紅螞蟻

* YesKno 時間@[Youtube](https://www.youtube.com/playlist?list=PLCMDemtx7aFLQI6cxzSdodF-O6AnhViXE) 與 [短影音-極短剪](https://youtube.com/playlist?list=PLCMDemtx7aFI2HsX1pFUnwBQN790-zu0P&si=Uck1RPQtQrLR0K2k)
* 秋意濃- 台灣之四季分明
* 水果、農產品與庭園植栽
* 我們家的堅毅苦楝 與 東華二期宿舍的梔子花
* 彩蛋你來唱
* 片尾 江江張張 唱的 [夜空下最亮的星](https://www.kkbox.com/tw/en/song/OkFg3NfnwySdIpeM_L)  原唱：[逃跑計劃](https://www.kkbox.com/tw/en/artist/5ZYjcWda-SSceAbBjl)

### Copyright Disclaimer
Cover art linked from [https://www.musicofvienna.com/vivaldi-four-seasons.htm](https://www.musicofvienna.com/vivaldi-four-seasons.htm)