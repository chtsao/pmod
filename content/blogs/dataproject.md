+++
title = 'Data Project Overview'
date = 2024-02-22T05:26:45+08:00
draft = false
tags=["data project", "project", "kno", "kbc"]
categories=["kbc"]
author=["kno"]
+++
<img src="https://blog.atinternet.com/wp-content/uploads/2021/06/DATAPROJECT-1536x1024.jpg" style="zoom: 33%;" />

## Q and A--- and more


### Project orientation: Questions/Problems
* [ykt. 24./kbc. 05. 你有問題嗎？--Data project: Questioning](https://chtsao.gitlab.io/pmod/posts/ep24.question.dataproj/)
* 不要急著回答，先確實弄清楚問題
### Presentation
* [amic 10. (kbc 03) Data Project: Presentation 01 ](https://chtsao.gitlab.io/pmod/posts/kbc03.dproj.pres/)

### Report writing
* [amic 8. (kbc 02) Data Project: Report/Thesis Writing](https://chtsao.gitlab.io/pmod/posts/kbc02.dproj.writing/)



#### Copyright disclaimers
Cover visual linked from [https://blog.atinternet.com/wp-content/uploads/2021/06/DATAPROJECT-1536x1024.jpg](https://blog.atinternet.com/wp-content/uploads/2021/06/DATAPROJECT-1536x1024.jpg)