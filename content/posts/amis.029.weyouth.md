+++
title = 'amis.029.系學會2024--我們青春'
date = 2024-11-08T21:53:53+08:00
draft = false
tags =["amic", "系學會", "迎新宿營","夜教","彩排"]
categories = ["amic"]
+++
![](https://chtsao.gitlab.io/pmod/youth.jpg)
:🎧 [先聽為快](https://open.spotify.com/show/6EOiyanrZTWWxpidZlIyb1?si=bdc4c9ce66be4b59)
:🗣️  [A麥開講,歡迎指教](https://forms.gle/N29BRtvDezbKo6Vx8)

## 系學會2024--我們青春

* 緣起 與 系學會幕前幕後
* 心聲 新生 成長
* 給學弟妹和同學們的心裡話
* 彩排 之 如何估算麵粉的用量 
* 夜教 迎新宿營 
* [系烤 11/12](https://www.facebook.com/photo?fbid=977430380859856&set=a.521857269750505)
* 感動的時刻 彩蛋 [我們青春](https://www.kkbox.com/tw/en/song/OrlqBrVVf78HUZZ0hy) 原唱：[李玉璽](https://www.kkbox.com/tw/en/artist/PaHOlATCBoelCBhbAn)

Cover art linked from [https://rumahpemilu.org/wp-content/uploads/2018/05/youth.jpg](https://rumahpemilu.org/wp-content/uploads/2018/05/youth.jpg)