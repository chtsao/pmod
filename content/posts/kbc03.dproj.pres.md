+++
title = 'amic 10./kbc 03. Data Project: Presentation 01'
date = 2024-02-01T06:18:26+08:00
draft = false
tags = ["amic", "kbc", "data project", "presentation"]
categories=["kbc", "amic"]
author = "Kno"
+++
![https://virtualspeech.com/blog/technical-presentation](https://virtualspeech.com/wp-content/uploads/presenting-in-english.jpg)


::headphones: [先聽為快](https://podcast.kkbox.com/episode/Skdn3P2TH0XpXZbN_P) 

### Constraints/Circumstances 
* 時間, TA (Target Audience)
* 類似網路文頁如 [Bookdown](https://bookdown.org/yihui/bookdown/) 的收/展 事前準備/安排呈現 
### 目標
當然是分享結果, 最後 propose/suggest actions. 但有更細的區分要注意
* 求職/行銷：自己/團隊
* 引介/推廣 Tech/方法
* 提出/引示 (過去不甚清楚/不知道的) 觀察/洞見
### Structure
* Outline

* Introduction: The Question and your answer (QDDA: Question, Data, Domain, Answer) $\Leftarrow$ Why? What? How?

* Results
  * Data: 資料來源, 背景與相關性
  * Figures/Tables: 主要的2-4個圖表
  * 方法/模型/選擇想法與原因說明
  
* Conclusion
  * Briefly and clearly, recap your main findings/results 
  * Highlight your contribution
### Do's and Don't's
  * Do's: 簡潔/減截；專注於重點；Clarification vs. Simplification
  * Don't's: 滿頁文字，讓人分心的不必要資訊, 無助了解/專注的圖文或效果
### 提醒/Tips
* 設身處地想想若是你是聽眾，你希望聽到什麼樣的演講，你又可以忍耐多少張投影片不分心
* 講者是主角$\neq$念稿人; 口頭報告文件$\neq$論文; 小抄/手卡
* Re, Re, Re: 熟悉/掌控時間與Tempo

### 補充資料
* 程式碼, 更多完整的說明；做出來的圖表 都可以放在另外的雲端供參考 如Github, Gitlab
* 碩/博士論文；技術報告等 

### Read and Links
* [Project Presentation Guides](https://chtsao.gitlab.io/sml22/posts/w15/)