+++
title = 'amic 8./kbc 02. Data Project: Report/Thesis Writing'
date = 2024-01-09T06:18:26+08:00
draft = false
tags = ["amic", "kbc", "data project", "writing"]
categories =["kbc","amic"]
author = "Kno"

+++
<img src="https://cmilearn.org/wp-content/uploads/2018/01/composition.jpg" style="zoom:30%;" />

::headphones: [先聽為快](https://podcast.kkbox.com/episode/PXIILvBZdrj-5CcqdO) 

### Constraints/Circumstances 
* Thesis, Tech shares, Evidence study/research (as a decision support)
* TA (Target Audience): Peer students, colleagues, researchers
### 目標
當然是分享結果, 最後 propose/suggest actions. 但有更細的區分要注意
* 求職/行銷：自己/團隊
* 引介/推廣 Tech/方法
* 提出/引示 (過去不甚清楚/不知道的) 觀察/洞見

作為你口頭或是其他較簡短形式報告的完整呈現，包含參考文獻，程式，完整分析/圖表等。
### Structure
* Abstract: 注意 abstract, introduction 以及 conclusion 之異同。
* Introduction: The Question and your answer (QDDA: Question, Data, Domain, Answer) $\Leftarrow$ Why? What? How?
* Formulation: How do you address the question? How do you formulate it as a ML/Stat Problem and why?
* Results
  * Data: 資料來源, 背景與相關性
  * Figures/Tables: 主要的2-4個圖表
  * 方法/模型/選擇
    * 想法與原因說明
    * 方法/模型 概念/algorithm/程式 implementation 簡介
  * 主要發現與結果之說明與小結
* Conclusion
  * Briefly and clearly, recap your main findings/results 
  * Highlight your contribution
* References
  * 不要輕忽這個部份，這是研究生學習中的重要主題。有效的研讀與了解，才能對研究主題有一個比較全面的了解。未來進行其他研究時，才知道如何開始/尋找主題等。
  * 格式統一
### Do's and Don't's
  * Do's: 簡潔/減截；專注於重點；Clarification vs. Simplification
  * Don't's: 流水帳/時間紀錄
### 提醒/Tips
* 設定好你的讀者 他們大概知道多少 以及你要讓他們新知道什麼
* Q and A 仍然是重中之重 不要讓細節困住讀者以及自己
* Grooving

### 補充資料
* 程式碼, 更多完整的說明；做出來的圖表 都可以放在另外的雲端供參考 如Github, Gitlab

### Read and Links
* [研究生完全求生手冊:方法、秘訣、潛規則](https://www.linkingbooks.com.tw/LNB/book/Book.aspx?ID=152126) by 彭明輝