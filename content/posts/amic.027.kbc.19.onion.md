+++
title = 'amic.027.kbc.19.教科書閱讀之延伸--技術內容閱讀'
date = 2024-09-20T11:56:32+08:00
draft = false
tags = ["amic", "kbc", "synthesis reading", "technical reading"]
categories=["kbc", "amic"]
author = "Kno"
+++
![](https://chtsao.gitlab.io/pmod/buffetreading.jpg)
:🎧 [先聽為快](https://open.spotify.com/show/6EOiyanrZTWWxpidZlIyb1?si=bdc4c9ce66be4b59)
:🗣️ [A麥開講,歡迎指教](https://forms.gle/N29BRtvDezbKo6Vx8)

## 教科書閱讀之延伸--技術內容閱讀

* [Synthesizing reading](https://owl.excelsior.edu/orc/what-to-do-after-reading/synthesizing/)
* 教科書閱讀：[SQ3R](https://www.globalcognition.org/sq3r/): Survey, Question, Read, Recite and Review
* 以吃 Buffet 的角度來讀技術文件
* 頭尾是重點，abstract, introduction, conclusion and result 章節
* 蘇東坡準備考試秘訣揭露; [蘇東坡傳：林語堂](https://www.haodoo.net/?M=u&P=C10U5:0&L=)
* 文件越多，越要抓清主軸，篩選/區分 相關與重要的文件及段落

Cover art linked from [https://www.teahub.io/viewwp/ixhhhmh_warren-buffett-reading/](https://www.teahub.io/viewwp/ixhhhmh_warren-buffett-reading/)
