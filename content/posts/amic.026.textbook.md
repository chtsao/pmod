+++
title = 'amic.026.數學原文教科書怎麼讀'
date = 2024-09-05T21:01:17+08:00
draft = false
tags =["amic", "textbook", "原文教科書", "學習"]
categories = ["amic"]
+++
![](https://chtsao.gitlab.io/pmod/textbooks.jpg)

:🎧 [先聽為快](https://open.spotify.com/show/6EOiyanrZTWWxpidZlIyb1?si=bdc4c9ce66be4b59)
:🗣️  [A麥開講,歡迎指教](https://forms.gle/N29BRtvDezbKo6Vx8)

## 數學原文教科書怎麼讀

* 原文教科書是上大學一開始學習要突破的而可以突破的門檻
* 教科書是使用手冊，不要逐字閱讀
* 抓住老師的教學路徑
* 了解教科書的結構：Content (Chapter, Section) and Index
* Yes無私大招分享：定義、粗體字、例子
* Kno怪招"破解"妳對教科書的恐懼
* 教科書是妳忠實的朋友，一直在身邊的老師

Cover art linked from [hhttps://www.teahub.io/viewwp/ixhhhmh_warren-buffett-reading/](https://www.teahub.io/viewwp/ixhhhmh_warren-buffett-reading/)
