+++
title = 'ykt.064.颱風--人地時事，花蓮'
date = 2024-11-02T08:12:53+08:00
draft = false
tags =["颱風","花蓮"]
categories = ["ykt"]
+++
![](https://chtsao.gitlab.io/pmod/typhoon.jpg)

:🎧 [先聽為快](https://open.spotify.com/show/1KlWNRx9QIu7m7JpqlIBhL?si=a1bd701af6a54627) : [Youtube](https://www.youtube.com/playlist?list=PLCMDemtx7aFLQI6cxzSdodF-O6AnhViXE) 與 [短影音-極短剪](https://youtube.com/playlist?list=PLCMDemtx7aFI2HsX1pFUnwBQN790-zu0P&si=Uck1RPQtQrLR0K2k)
🗣️  [YesKno時間 歡迎指教](https://forms.gle/4Qvjv5Ui63Nveark7)

## 颱風--人地時事，花蓮

* YesKno 時間上 [Youtube](https://www.youtube.com/playlist?list=PLCMDemtx7aFLQI6cxzSdodF-O6AnhViXE) 與 [短影音-極短剪](https://youtube.com/playlist?list=PLCMDemtx7aFI2HsX1pFUnwBQN790-zu0P&si=Uck1RPQtQrLR0K2k)
* 記憶中的颱風
* 花蓮的颱風
* 風中的白鷺鷥
* 天佑花蓮 天佑台灣
* 片尾 江江 唱的 [奉獻](https://www.kkbox.com/tw/en/song/8tBi2TejsnPsHLGDSZ)  原唱：[蘇芮](https://www.kkbox.com/tw/en/artist/9ahmgD_q8L4InyBwoC)

### Copyright Disclaimer
Cover art linked from [https://rdc28.cwa.gov.tw/TDB/public/typhoon_detail?typhoon_id=202421](https://rdc28.cwa.gov.tw/TDB/public/typhoon_detail?typhoon_id=202421)