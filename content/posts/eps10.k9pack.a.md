+++
title = 'ykt 10. 我們這一群: YK和他們的狗狗們 (上)'
date = 2024-01-10T08:23:47+08:00
draft = false
tags = ["dogs","life"]
+++

![](https://chtsao.gitlab.io/pmod/lala.yammy.jpg)

[先聽為快](https://player.soundon.fm/p/43e61ad9-d1bf-43f1-aebe-fb5f502bad61/episodes/bf00ae32-b740-4f83-b211-24eadba58964)

狗寶貝們是如何找到YK的? 牠們如何有效地訓練 YK?

* 小狗七十二變？
* 狗爸狗媽經分享
* 狗眼看世界

### 參考閱讀
*  [當人遇見狗](https://www.eslite.com/product/1001113971416630) by 康拉德.勞倫斯
*  [How to Speak Dog](https://www.amazon.com/How-Speak-Dog-Mastering-Communication/dp/074320297X) by Stanley Coren
* [The Intelligence of Dogs](https://www.amazon.com/-/zh_TW/Stanley-Coren/dp/0743280873/ref=sr_1_1?crid=3T5ONO62EUTZV&qid=1704847126&s=books&sprefix=intelligence%20of%20dog,stripbooks-intl-ship,257) by Stanley Coren

