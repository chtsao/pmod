---
title: "About"
date: 2023-12-15T05:13:17+08:00
draft: false
categories: [adm]
---
<img src="http://www.conspirazzi.com/wp-content/uploads/2012/07/try/aleph-roman-greek.jpg"  />

## YesKno 時間

* [YesKno 時間](https://player.soundon.fm/p/43e61ad9-d1bf-43f1-aebe-fb5f502bad61) =  Yes 與 Kno 的 1st podcast hack.
  * hack = 且學且做； 交流（換）學習/分享/共學/共享
  * Yes, Kno... Who?: [Yes, Kno 自介](https://player.soundon.fm/p/43e61ad9-d1bf-43f1-aebe-fb5f502bad61/episodes/01894988-9959-4df3-9978-36177d6ab5dc)

### 版權與內容使用
* YesKno 時間 在我們構創中是一個不以營利為目的 podcast，分類標籤是教育，但本質更是分享與交流。
* 我們是新手 podcaster, 透過實做來了解學習 podcasting 以及 podcastverse. 學習階段預期的錯誤與不當，我們知道後會修正改進。還請諒解。
* 本網站 YesKnotes 內容中包含網路取得圖片/影片部份是以 Hugo 以 Markdown 語法取得，直接連至該圖片/影片。除特別聲明，不另外下載。在呈現時，可以由 url 直接知道該物件的連結/所有者。該內容版權為原版權所有人擁有。若在使用/呈現方式上違反版權法，還請見諒告知。我們會立即修正更改。
* YesKno 時間中之 演唱歌曲或使用音樂之對應版權 為原作曲/作詞/出版Label/公司 所有。同樣地，若在使用/呈現方式上違反版權法，還請見諒告知。我們會立即修正改善。我們的演唱/演奏是非營利本質，歡迎聆聽/分享。若要做商業營利使用，還煩請通知讓我們知道。

#### 歡迎大家和我們一起共享 **YesKno 時間 **！也歡迎各位的指教！