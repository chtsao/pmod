+++
title = 'ykt.053.kbc18.高績效心智-如何學到做到聰明工作？'
date = 2024-08-16T01:45:45+08:00
draft =false
categories=["kbc", "ykt"]
tags= ["聰明工作", "努力", "績效","project"]
author=["Kno"]

+++
![](https://chtsao.gitlab.io/pmod/greenlandsleddogs_036_at.jpg)


:🎧 [先聽為快](https://open.spotify.com/show/1KlWNRx9QIu7m7JpqlIBhL?si=a1bd701af6a54627) :  🗣️  [YesKno時間 歡迎指教](https://forms.gle/4Qvjv5Ui63Nveark7)

# 高績效心智-如何學到做到聰明工作？

* 娜塔莉之謎
* 配合度高 什麼都做 長時工作 努力再努力---可能是你的大錯誤
* 不要把努力當作習慣 
* 關於聰明工作的實證研究結果--高績效者擁有的七個心智
* 個人面向：雙重專注 學習迴圈
* 征服南極點的競賽：Scott vs. Amundsen
* [高績效心智/Great at Work](https://bookzone.cwgv.com.tw/book/BCB814) by Mortan Hansen. 天下/遠見出版
#### Copyright disclaimer
Visual borrowed from [https://albatros-arctic-circle.com/dog-sledding-tour-4-hours](https://albatros-arctic-circle.com/sites/default/files/styles/header_desktop/public/images/greenland_westgreenland_kangerlussuaq_sleddogs_036_at.jpg?itok=SaWOmXBX)
