+++
title = 'ykt.072.年底盤點：評量自己的幸福感、滿意度與快樂值'
date = 2024-12-27T19:35:10+08:00
draft = false
categories = ["YesKno"]
tags = ["ykt", "","盤點","career","social", "financial", "community", "health"]
+++
![](https://chtsao.gitlab.io/pmod/Janus.jpg)

🗣️  [YesKno時間 歡迎指教](https://forms.gle/4Qvjv5Ui63Nveark7)

## 年底盤點：評量自己的幸福感、滿意度與快樂值

* YesKno 時間@[Youtube](https://www.youtube.com/playlist?list=PLCMDemtx7aFLQI6cxzSdodF-O6AnhViXE) 與 [短影音-極短剪](https://youtube.com/playlist?list=PLCMDemtx7aFI2HsX1pFUnwBQN790-zu0P&si=Uck1RPQtQrLR0K2k)
* 年底盤點：自我滿意度、幸福感與快樂值評量: CaSeFetCH
* [2024.12.23 理財生活通/夏韻芬：年底做財務盤眼時，如何避免面對財務焦慮。陳敏莉專訪](https://open.spotify.com/episode/0cWwex9rVNyejRbPV5PPvU?si=wKdjiv8IRE2m7K_Kankcjg)
* Career：職涯、工作
* Social： 關係、親人、家庭
* Financial：短中長期目標；安全感、安全度
* Community： 社區、服務、貢獻、相互幫助
* Health：基本之基本，照顧好自己，才能照顧好別人
* 花絮：野地的花

### Copyright Disclaimer
Cover art linked from [https://en.wikipedia.org/wiki/File:Janus-Vatican.JPG](https://en.wikipedia.org/wiki/File:Janus-Vatican.JPG)

