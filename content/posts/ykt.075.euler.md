+++
title = 'ykt.075.尤拉公式'
date = 2025-01-18T06:38:52+08:00
draft  = false
categories = ["YesKno"]
tags = ["ykt",  "Euler's formula", "e", "pi", "calculus","Taylor expansion"]

+++

![](https://chtsao.gitlab.io/pmod/euler.jpg)

🗣️  [YesKno時間 歡迎指教](https://forms.gle/4Qvjv5Ui63Nveark7)

## 尤拉公式

* YesKno 時間@[Youtube](https://www.youtube.com/playlist?list=PLCMDemtx7aFLQI6cxzSdodF-O6AnhViXE) 與 [短影音-極短剪](https://youtube.com/playlist?list=PLCMDemtx7aFI2HsX1pFUnwBQN790-zu0P&si=Uck1RPQtQrLR0K2k)
* $e^{i x} = \cos(x) + i \sin(x)$
* $e = \lim_{n \rightarrow \infty} (1+ \frac{1}{n})^n.$
* 毛起來說 e：e: The Story of a Number。天下文化。 Eli Maor；  譯者: 鄭惟厚。
* 泰勒展開式 
* [Euler’s Formula: A Complete Guide](https://mathvault.ca/euler-formula/)
* 花絮：微光中的歌吟，鄭怡。

### Copyright Disclaimer
Cover visual  linked from [https://www.tshirtgeek.com.br/loja/matematica/formula-de-euler/](https://www.tshirtgeek.com.br/loja/matematica/formula-de-euler/)