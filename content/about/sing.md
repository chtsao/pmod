+++
title = '我們唱的歌'
date = 2023-12-16T02:36:34+08:00
draft = false
tag = ["music"]
+++
<img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fi.ytimg.com%2Fvi%2F4MUCOjn6pQ8%2Fmaxresdefault.jpg&f=1&nofb=1&ipt=856e7135f1326df8fb15bbae42d3cf99ba4e0351adb8f1c76fdc5d08583bdd1f&ipo=images" style="zoom:47%;" />

我們喜歡唱歌，YesKno 時間 我們當然唱歌。為紀錄也為版權使用清楚，在這裡條列我們唱的歌曲。重申我們只是非商業營利 podcast, cover 一些我們喜歡的歌曲。如有侵權或版權方面的疑慮，也請讓我們知道改正。另外，考量給聽眾一些期待，歌曲將在當集上線*之後*約 3 天之後在此列出。

### 歌曲
1. [閉上雙眼的時候](https://www.kkbox.com/tw/en/song/Wl5ltj6BHya3ACvjzl) by [劉容嘉](https://www.kkbox.com/tw/en/artist/OttauChrlH1BP_rvbU)
2. [雪落下的聲音](https://youtu.be/UuTBI3MY_ic?feature=shared) covered by [李千娜](https://www.kkbox.com/tw/en/artist/WtZEndUWvn57Wt8tEr)。 原唱：陸虎
3. [蚵仔麵線](https://www.kkbox.com/tw/en/song/HXBY2LQIaXJ8b1Q6wV) by [琳誼 Ring](https://www.kkbox.com/tw/en/artist/CoMDZ5uddysV05z0QF)
4. [菊花臺](https://www.kkbox.com/tw/en/song/KoUkMXehP2VFv2qMLi) by  [周杰倫](https://www.kkbox.com/tw/en/artist/GtjT_E-Fw6HSCE7jgQ)
5. [小城故事/鄧麗君組曲合唱版(高竹嵐編曲)](https://youtu.be/2qJNGMH5sdw?feature=shared) inspired  yk variation.
6. [這世界那麼多人](https://www.kkbox.com/tw/en/song/0lbKMt6Xs1XUcxAjVf) by [莫文蔚](https://www.kkbox.com/tw/en/artist/KlYdCzYxd1n-CHQpYn)
7. [喜歡我吧](https://www.kkbox.com/tw/en/song/9al85ZW91jmuLsblvK) by [魏嘉瑩](https://www.kkbox.com/tw/en/artist/LZgVM3V1w4lbhltaNH)
8. [禮物](https://www.kkbox.com/tw/en/song/H_wjQn0I6Jmyw-bpT7) by [淺堤](https://www.kkbox.com/tw/en/artist/-t4BIpTrdRktDZiY5l)；[Wi ya suni nu vali du ya la vi i]() by [檳榔兄弟]() @[Ep2. A麥開講](https://open.spotify.com/episode/6EYLNN5YxfhFSmFhywduUk?si=umr6_iaTTBqZo8qwmq4GNg) 
9. [行過](https://www.kkbox.com/tw/en/song/Ct_J1UalUSEGYVFW_I) by [KIM (邱廉欽)](https://www.kkbox.com/tw/en/artist/4tlIwMjJ8jt7GgZYPh) 
10. [好野](https://www.kkbox.com/tw/en/song/Opn1IDCAzUmSa8JndN) by [宇宙人](https://www.kkbox.com/tw/en/artist/Lai8PoHfMgHOt_XWaz)  
11. [巧合](https://www.kkbox.com/tw/en/song/8p3T9xcb2c2ekse-4K) by [鳳飛飛](https://www.kkbox.com/tw/en/artist/4nrdwg-K2KLL6xgk7-)
12. [麻雀](https://www.kkbox.com/tw/en/song/XXhT7KRIN4kbpwjjN0) by [李榮浩](https://www.kkbox.com/tw/en/artist/D-0uSsbo3KbdjIQkWA)
13. [路過人間](https://www.kkbox.com/tw/en/song/D-5BjfCWldMimAJDGF) by [郁可唯](https://www.kkbox.com/tw/en/artist/5ZNNwWda-SScf13oj0) 
14. [隱形的翅膀](https://www.kkbox.com/tw/en/song/SkJuZPKGFd6eC_FzJW) by [張韶涵 (Angela Chang)](https://www.kkbox.com/tw/en/artist/4tkLYujJ8jt7EXGlr6)
15. [祝我幸福](https://www.kkbox.com/tw/en/song/Os5aQxpE8UtswBMnt8) by [楊乃文 (Naiwen Yang)](https://www.kkbox.com/tw/en/artist/GpACqbPEBUDsifn1RE)
16. [暖暖](https://www.kkbox.com/tw/en/song/1_ZvXfuk_TKl5LpA0S) by [梁靜茹 (Fish Leong)](https://www.kkbox.com/tw/en/artist/P-Fv-nmft87W26PYBe)
17. [我不是神，我只是平凡卻直拗愛著你的人 (Wordless Groans)](https://www.kkbox.com/tw/en/song/5-KZH2S-i4L5osKi8Z) by [林宥嘉 (Yoga Lin)](https://www.kkbox.com/tw/en/artist/4maQaK6fNyolbxCHib)
18. [恭喜發財](https://www.kkbox.com/tw/en/song/XXkB_XWoN4kbqLrfv5) 
19. [跟你住](https://www.kkbox.com/tw/en/song/-nPYwwI4z-Jygf_uYf) by [孫盛希 (Shi Shi)](https://www.kkbox.com/tw/en/artist/Gl9yFo8JxUb9_2NeXR)
20. [不喜歡補習班](https://www.kkbox.com/tw/en/song/8tOynJejsnPsEui4xN) by [王大文](https://www.kkbox.com/tw/en/artist/Onec_K4Ue0DkN8-rL8)
21. [生日快樂歌] in Faroese 
22. [神秘谷之歌](https://www.kkbox.com/tw/en/song/_ZsySrmVncpGNUWVKJ) Sung by [溫梅桂, 張秀美](https://www.kkbox.com/tw/en/artist/5-ctjlVc8AiCLmyt3t); [【S1EP48】讓耳朵來說 - 神祕谷之聲(太魯閣族語版)｜Putang Nuku｜太魯閣族語](https://podcast.kkbox.com/episode/Gtt9uYbaXPlN2qiz4W) 
23. ykt. 30. [愛情](https://www.kkbox.com/tw/en/song/Kssr1nw3fpSTT_6oRn) by  [莫文蔚](https://www.kkbox.com/tw/en/artist/KlYdCzYxd1n-CHQpYn)
24. ykt.32. [說散就散](https://www.kkbox.com/tw/en/song/9_nDADq-L75jJoawYS) by [JC 陳詠桐](https://www.kkbox.com/tw/en/artist/1X3B6z-uTCtJwhpU32)
25. ykt. 35. [阿拉斯加海灣](https://www.kkbox.com/tw/en/song/CpvIr_efXVWGs1sJVK) by [蔡恩雨 (Priscilla Abby)](https://www.kkbox.com/tw/en/artist/0kYVYnrRCS2ArjdfGj). 
26. ykt.43. [糸](https://www.kkbox.com/tw/en/song/4lQbjjnq9Z5EZQeZx6) by [中島美雪](https://www.kkbox.com/tw/en/artist/Daxv-PBJ8qnEuBMH-k)
27. ykt.45. [稻香](https://www.kkbox.com/tw/en/song/SoDdrzn81DbhbHDJOk) by [周杰倫](https://www.kkbox.com/tw/en/artist/GtjT_E-Fw6HSCE7jgQ). Cover: 張張 江江 Yes
28. ykt.46. [不是因為天氣晴朗才愛你](https://www.kkbox.com/tw/en/song/GltcN6bJylVNFrIEmc) by [理想混蛋](https://www.kkbox.com/tw/en/artist/9a3hK2_q8L4InZ6yYs). Cover: Lynn, Ray and Yes
29. ykt.47. [小幸運](https://www.kkbox.com/tw/en/song/WsHf0h5WZsiE9Q7nfT) by [田馥甄 (Hebe)](https://www.kkbox.com/tw/en/artist/Xa_WYdZHejH-lBzV-F): Cover: Yes, 張張, 江江
30. amic.15. [城裡的月光](https://www.kkbox.com/tw/en/song/5avI29KjyKyO5L-Lt9) by [許美靜](https://www.kkbox.com/tw/en/artist/9Xd-jyg5vg3gZ6nwRI). Cover: David, Lynn, Yes.
31. amic.18. 綠島小夜曲. Cover: David, Lynn, Yes.
32. ykt.49. [長空下的獨白](https://www.kkbox.com/tw/en/song/1-BIgqVXWDhid8NBfh) by [包美聖](https://www.kkbox.com/tw/en/artist/Gtqw7E-Fw6HSDK_mFI)。
33. ykt.50. [償還](https://www.kkbox.com/tw/en/song/OoPV7X9a32AqZpbEh6) by [鄧麗君](https://www.kkbox.com/tw/en/artist/HX4pQTKP8cmeHPxcfh) 
34. ykt.52. [夜空下最亮的星](https://www.kkbox.com/tw/en/song/OkFg3NfnwySdIpeM_L) by [逃跑計畫](https://www.kkbox.com/tw/en/artist/5ZYjcWda-SSceAbBjl) covered by 張張、江江
35. ykt.54. [淚光閃閃](https://www.kkbox.com/tw/en/song/8sd1tI7NV0ZqmsCDsu) by [夏川里美](https://www.kkbox.com/tw/en/artist/Sm59q0lG4jjF1AuFWG)
36. ykt.55. [Autumn in a Village](https://www.kkbox.com/tw/en/song/D-7Yb6XmldMinDWfq4) by [Yoshikazu Mera, Natsuko Uchiyama](https://www.kkbox.com/tw/en/artist/0qHRWrVbPu5uv-jPcU)
37. ykt.56. [浜辺の歌](https://www.kkbox.com/tw/en/song/9-UczeKvF6qQ2okLIo) by [由紀さおり, 安田祥子](https://www.kkbox.com/tw/en/artist/Csh2xyo952obVYkiOm)
38. ykt.57. [君といつまでも](https://youtu.be/aLs0GTQjzXc?feature=shared) by 加山雄三
39. ykt.58. [下雨的夜晚](https://www.kkbox.com/tw/en/song/OqJcnM3vMMmVfq7dgr) by [蘇打綠](https://www.kkbox.com/tw/en/artist/DaVqiPBJ8qnEtzTaSU)
40. ykt.59. [下午的一齣戲](https://www.kkbox.com/tw/en/song/OmgldXe9TsIBfL2JIG) by [陳明章](https://www.kkbox.com/tw/en/artist/8tY93OpHItW_iQ0Q-G)
41. ykt.60. [陽光和小雨](https://www.kkbox.com/tw/en/song/T_pi5mGJ6ZmfX6SSD3) by [銀霞](https://www.kkbox.com/tw/en/artist/HYXbBt2oa-MNVUhc4I)
42. ykt.61. [遠走高飛](https://www.kkbox.com/tw/en/song/8mM0A1FQJRsamJiXXD) by [江蕙](https://www.kkbox.com/tw/en/artist/Csw6Xao952obVfdTkB).
43. ykt.62. 浮雲遊子 by 陳明韶
44. ykt.63. 無情人請妳離開 by 江蕙
45. ykt.64. 江江 唱的 奉獻 原唱：蘇芮
46. ykt.65. 江江張張 唱的 夜空下最亮的星 原唱：逃跑計劃
47. ykt.66. 如鹿渴慕溪水 (As the Deer).
48. ykt.67. 掌聲響起 by 鳳飛飛
49. ykt.68. 花心 by 周華健
50. ykt.69. kbc.
51. ykt.70. 我家在那裡 by 劉家昌
52. ykt.71. [蘇三起解](https://www.kkbox.com/tw/en/song/0l170H4ns1XUe5LbbZ) by [李翊君](https://www.kkbox.com/tw/en/artist/5ZchIWda-SSccF3bBn)
53. ykt.72. 野地的花
54. ykt.73. 茉莉花
55. ykt.74. 吉他演奏：南方,寂寞火車 陳明章作曲
56. ykt.75. 微光中的歌吟，鄭怡。
57. ykt.76. kbc
57. ykt.77. 咚咚龍鏹
57. ykt.78. kbc
57. ykt.79. [驛動的心](https://play.kkbox.com/track/KoUG5IeBP2VFsl3Sea), [姜育恆](https://play.kkbox.com/artist/CqFpjCAUuRkVbaWON-)
### 音樂/音段使用
* #ykt 音段 kno 自作~~自受~~自彈
* K書仲心 #kbc 頭尾音效 使用 [Guitar Intro by MeszarcsekGergely](https://pixabay.com/sound-effects/guitar-intro-110935/) Free for use under the Pixabay [Content License](https://pixabay.com/service/license-summary/)
