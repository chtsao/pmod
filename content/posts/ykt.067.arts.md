+++
title = 'ykt.067.游於藝'
date = 2024-11-23T09:05:06+08:00
draft = false
categories = ["YesKno"]
tags = ["ykt", "藝術","現場","展演"]
+++
![](https://chtsao.gitlab.io/pmod/arts.jpg)

:🎧 [先聽為快](https://open.spotify.com/show/1KlWNRx9QIu7m7JpqlIBhL?si=a1bd701af6a54627) : [Youtube](https://www.youtube.com/playlist?list=PLCMDemtx7aFLQI6cxzSdodF-O6AnhViXE) 與 [短影音-極短剪](https://youtube.com/playlist?list=PLCMDemtx7aFI2HsX1pFUnwBQN790-zu0P&si=Uck1RPQtQrLR0K2k)
🗣️  [YesKno時間 歡迎指教](https://forms.gle/4Qvjv5Ui63Nveark7)

## 游於藝

* YesKno 時間@[Youtube](https://www.youtube.com/playlist?list=PLCMDemtx7aFLQI6cxzSdodF-O6AnhViXE) 與 [短影音-極短剪](https://youtube.com/playlist?list=PLCMDemtx7aFI2HsX1pFUnwBQN790-zu0P&si=Uck1RPQtQrLR0K2k)
* [志於道，據於德，依於仁，游於藝](https://chinese.bookmarks.tw/poetry/170)
* 藝文活動，音樂表演，展演
* Clarinet 高手 大學交響樂團
* [Sonatas and Parititas for Solo Violin, BWV 1001–1006]() by Bach, Violinist: [Nathan Milstein](https://www.kkbox.com/tw/en/artist/8nAsa0dUM64-cghXJO) in [The Art of Nathan Milstein](https://www.kkbox.com/tw/en/album/5aDy-LxIczZYSDnnab). [Vesselin Paraschkevov](https://www.paraschkevov.de/en-gb/home) [【音樂會】前維也納愛樂首席費瑟-巴哈無伴奏之夜](https://music.ndhu.edu.tw/p/406-1072-18665,r3061.php?Lang=zh-tw)
* 現場的 聽覺 視覺 五官六識 的體驗 
* [一曲難忘的現代舞：Bolero](https://mhperng.blogspot.com/2012/05/bolero.html)
* 上課與策展 [蔓生流域](https://ip-gallery.com/%E8%8A%B1%E8%93%AE%E9%A4%A8%2F%E5%BE%A9%E8%88%8855) [莊臥龍](https://ip-gallery.com/%E8%8E%8A%E8%87%A5%E9%BE%8D)
* 音樂了解我們 [楊照 遊樂之心--打開耳朵聽音樂](https://www.books.com.tw/products/0010657408)
* 花絮：[掌聲響起](https://www.kkbox.com/tw/en/song/8qHZPvJ8YrrZNoMI25) by [鳳飛飛](https://www.kkbox.com/tw/en/artist/4nrdwg-K2KLL6xgk7-) 

### Copyright Disclaimer
Cover art linked from [https://www.zastavki.com/rus/Creative_Wallpaper/wallpaper-30499.htm](https://www.zastavki.com/rus/Creative_Wallpaper/wallpaper-30499.htm)