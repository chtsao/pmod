+++
title = 'ykt.080.好佳哉--幸好哲學'
date = 2025-03-07T19:05:35+08:00
draft = false
categories = ["YesKno"]
tags = ["ykt",  "好佳在", "幸好","職涯"]
+++
<img src="https://chtsao.gitlab.io/pmod/silver.jpg" style="zoom:200%;" />

🗣️  [YesKno時間 歡迎指教](https://forms.gle/4Qvjv5Ui63Nveark7)

## 好佳哉 --幸好哲學

* YesKno 時間@[Youtube](https://www.youtube.com/playlist?list=PLCMDemtx7aFLQI6cxzSdodF-O6AnhViXE) 與 [短影音-極短剪](https://youtube.com/playlist?list=PLCMDemtx7aFI2HsX1pFUnwBQN790-zu0P&si=Uck1RPQtQrLR0K2k)
* 劉秀枝醫師：好佳哉--幸好哲學
* [劉秀枝專訪](https://open.spotify.com/episode/2ZnTXkS0XbhTPJvQaNynAN?si=SsYgKxAJQ-GgNhEL9KCtfg)@[名人書房 podcast](https://open.spotify.com/show/5FJOAregiTMI2ataTZ8R3f)
* 不要執著於選擇最好，但努力把你的選擇變成很好
* [陳亮恭專訪](https://open.spotify.com/episode/2fJj7NYDwehFSvBBqSfTcH?si=DtzBQwD0RcGM8ubpCmVWxA)
* [大腦韌性](https://blog.fnaith.com/2021/08/15/keep-sharp-build-a-better-brain/)
* 花絮：望春風

### Copyright Disclaimer
Cover visual  linked from [https://www.alustforlife.com/tools/mental-health/does-every-cloud-really-have-a-silver-lining](https://www.alustforlife.com/tools/mental-health/does-every-cloud-really-have-a-silver-lining)

