+++
title = 'ykt 8. 日曆變薄的時候'
date = 2023-12-29T18:59:43+08:00
draft = false
tags = ["成長", "演算法"]
+++
<img src="https://cdn.britannica.com/10/182610-050-77811599.jpg" style="zoom: 33%;" />

[先聽為快](https://player.soundon.fm/p/43e61ad9-d1bf-43f1-aebe-fb5f502bad61/episodes/f7d4e0c5-97ae-4a6c-bf2a-030c310d0e61)

* 舊年末，新年初：日曆變薄的時候
* 反省、提醒---但也不用想太多，過份計畫
* 演算法？ Random variable realization?
* 我們這一群人
* Kno 變魔術
* [Don't Worry; Be Happy](https://www.kkbox.com/tw/en/song/4nsqOB0I3TYuoPDZoA) by [Bobby McFerrin](https://www.kkbox.com/tw/en/artist/9ZLxo3i6Jj_jLeE7e0)