+++
title = 'ykt.055.從學唱一首歌開始'
date = 2024-08-30T04:42:05+08:00
draft =  false
tags =["ykt", "唱歌", "合唱", "貝多芬第九交響曲"]
categories = ["ykt"]
+++
![](https://chtsao.gitlab.io/pmod/beethovan.jpg)

:🎧 [先聽為快](https://open.spotify.com/show/1KlWNRx9QIu7m7JpqlIBhL?si=a1bd701af6a54627) :  🗣️  [YesKno時間 歡迎指教](https://forms.gle/4Qvjv5Ui63Nveark7)

## 從學唱一首歌開始

* Yes 學唱日語歌
* 學會到會唱一首歌
* 歌曲的結構分析
* 在合唱中學到的事
* 貝多芬第九交響曲終章

Cover art linked from [https://maiscousasdeavelino.blogspot.com/2020/05/adapting-beethoven.html](https://maiscousasdeavelino.blogspot.com/2020/05/adapting-beethoven.html)