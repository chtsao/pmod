+++
title = 'ykt. 035. 404 Error'
date = 2024-04-13T02:42:30+08:00
draft = false
tags =["ykt", "reset", "東華","花蓮"]
categories = ["ykt"]
+++
![](https://chtsao.gitlab.io/pmod/how-to-fix-error-404-1.webp)

:🎧 [先聽為快](https://player.soundon.fm/p/43e61ad9-d1bf-43f1-aebe-fb5f502bad61/episodes/f9e032b3-07f2-4af5-b823-5b3d542e58af) :  🗣️  [YesKno時間 歡迎指教](https://forms.gle/DJ84ppTfhqU3q7QRA)

## 404 Error 重新出發

大家小心、平安！ 重整後再出發！

* [404 Error](https://en.wikipedia.org/wiki/HTTP_404)
* [403大地震](https://zh.wikipedia.org/wiki/2024%E5%B9%B4%E8%8A%B1%E8%93%AE%E5%9C%B0%E9%9C%87)
* 關於 硬碟之 三態？[SSD vs. HDD](https://www.pcmag.com/news/ssd-vs-hdd-whats-the-difference)
* [九死南荒吾不恨，茲游奇絕冠平生: 蘇軾: 六月二十日夜渡海](https://www.arteducation.com.tw/shiwenv_f329f4ec0160.html).  抱歉，此處 Kno 所說有誤。這是東坡先生 1100年 遇赦，渡海北歸所作。並非方受貶時所作。
* [天地不仁，以萬物為芻狗: 老子道德經：虛用第五](https://www.chineseclassic.com/content/1618). 此處 芻狗 是以第二種解釋，即祭祀祈福之用的「草狗」。
* [白頭宮女在，閒坐說玄宗：唐 元稹 《行宮》](https://www.arteducation.com.tw/mingju/juv_5a5cd047be88.html) [玄武門之變](https://zh.wikipedia.org/zh-tw/%E7%8E%84%E6%AD%A6%E9%97%A8%E4%B9%8B%E5%8F%98)
* 片尾歌曲演唱： Yes and Lynn. 

#### Copyright disclaimer

Cover art linked from [https://www.hostinger.com/tutorials/wp-content/uploads/sites/2/2018/06/how-to-fix-error-404-1.webp](https://www.hostinger.com/tutorials/wp-content/uploads/sites/2/2018/06/how-to-fix-error-404-1.webp)